## Waveshare e-Paper Display - Programming an nRF52

### Demo App

To get a quick overview of how to use the demo, watch [this video on YouTube](https://youtu.be/rmvfcMDdhVQ)

The video shows the display connected to an nRF52 DK board, but it's quite easy to use any other nRF52832 based module to reproduce the demo as long as you map the pins accordingly. [See later section]

The code provided in this repository shows three demo screens:
* Screen 0 - Write specified text at location (15, 70) in 24pt font
* Screen 1 - Draw a filled and unfilled circle in white on a black background and write the Bluetooth advertised device name in 24pt font
* Screen 2 - Render monochrome image of size 200x200 on to display

The screen to be displayed is selected by writing the values 0, 1 or 2 (corresponding to the screen number above) into a single byte Bluetooth LE characteristic. The demo video shows this in action using the LightBlue Explorer BLE app.

#### Compiling the code
If you are not familiar with building code for the nRF52 platform, read on. Otherwise you can skip to the section [xxx].
The nRF5x microcontrollers are unique in the way code is structured since some of the functionality of the chip is exposed through a pre-compiled library called the Softcore. Although the library is a binary blob, you do need the header files to access the exposed APIs. Nordic Semiconductors also provides header files and standard libraries at multiple levels to ease process of developing software for the chip. All this code is made available in SDK libraries that Nordic releases every so often. The easiest way to start developing an app for the chip is to figure out what kind of functionality you will need, find an example in the 'examples' folder of the SDK, create a copy for your project, and modify the example as needed to build your own app.

##### Placing the files
This project is built using the nRF5 SDK 13.1.0. It is possible to build it for other SDK versions, of course, but doing that will require quite a few changes that are outside the scope of this document. So, from here onwards, I will assume that you have downloaded SDK version 13.1.0. You should then place the files from this project under \$\(SDK root\)/examples/ble_peripheral. For example, if your SDK files are in ~/nRF5_SDK_13.1.0 and you name the project folder 'epaperd_gfx', your main.c would be in ~/nRF5_SDK_13.1.0/examples/ble_peripheral/epaperd_gfx/main.c

To build the project, switch to the pca10040/s132/armgcc folder and issue 'make', followed by 'make flash_softcore' (you only need to do this once), followed by 'make flash'.

##### Build Tools Needed
For 'make' to succeed, you need to point your SDK Makefile.posix (or Makefile.windows) to the correct location of **gcc-arm** build toolchain. You can find Makefile.posix in (SDK root)/components/toolchain/gcc. Within the file, you need to update the build variable 'GNU_INSTALL_ROOT' to point to the folder that contains 'arm-none-eabi-gcc' and other binaries.
Besides the gcc-arm toolchain, you need a reference to the nrfjprog executable and the Segger JLink toolkit. I won't go into the details of installing gcc-arm, **nrfjprog** or **Segger JLink** here, since such details change over time, but if there is enough interest, I will describe in a separate document how I did it on MacOS 10.14 (Mojave). Once you have nrfjprog and Segger JLink installed, make sure you update the PATH environment variable to point to the directories that hold the corresponding executables, otherwise 'make flash' will fail.

### How the code works

The code is based on the nRF5 SDK ble_peripheral/blinky example code. I have removed references to the 'LED Button service' library and added on my own hooks for the BLE 'eink service' in addition to the Nordic GFX library with custom callbacks for the 1.54" Waveshare e-Paper display.

#### Pin Mapping
The nRF52 DK exposes all the 31 configurable I/O pins of the microcontroller on the expansion headers, but a set of them are assigned to functions on the board and require traces to be cut to reassign to external I/O. So, the demo uses the set of pins that are unassigned. Here is the mapping I used and configured in pca10040/s132/config/sdk_config.h as macro defines:

```C
#define WSEPD_SPI_MOSI  31
#define WSEPD_SPI_MISO  27
#define WSEPD_SPI_CLK   30
#define WSEPD_SPI_CS    29
#define WSEPD_DC        28
#define WSEPD_RST       4
#define WSEPD_BUSY      3
```

I have used the same names for the macros as the ones on the e-Paper module cable, so the cabling should be obvious. The numbers in the macro definitions correspond to the configurable pin number on the nRF52832 (P0.xx in the nRF52832 Product Specification document). The one extra pin configured here - the SPI_MISO - doesn't get hooked up in hardware to anything, but is needed for the SPI module to work properly.

#### Initialization
The only specific startup initialization I have added to the base code is for the BLE eink service. My custom implementation of `services_init()` looks like this:

```C
static void services_init(void)
{
    ret_code_t     err_code;
    ble_eink_init_t init;
    init.disptxt_write_handler = disptxt_write_handler;
    init.demoscr_write_handler = demoscr_write_handler;
    init.disptxt_buffer = disptxt_buffer;
    err_code = einksvc_init(&m_eink, &init);
    APP_ERROR_CHECK(err_code);
}
```

The function `einksvc_init()` is defined in the file einksvc.c.

The e-paper display doesn't need any initialization in the way I have implemented the demo. In the spirit of e-Paper use cases, I have designed it with the assumption that writing to the display is a rare event. The only hardware initialization you need to do on the nRF52 is the SPI channel setup along with the corresponding GPIO lines. This can be done on the fly every time the display is initialized before writing to it. Right after the display is written to, the display is put to sleep and the SPI and GPIO lines are released. This initialization is done in `hardware_init()` defined in waveshare_epd.c. This is a callback used by the NRF GPX library when you call `nrf_gpx_init()`.


#### Setting up the BLE Service and Characteristics
The details of setting up a BLE service with custom characteristics is outside the scope of this document, but you can refer to Nordic's Application Note 36 - Creating Bluetooth® Low Energy Applications Using nRF51822. The description below summarizes the code that sets up the service and its characteristics.

The BLE eink service is initialized from `einksvc_init()` function in einksvc.c. It defines a service with the UUID combination EINK_UUID_BASE + EINK_UUID_SERVICE, both of which are defined in einksvc.h. The service is registered with the BLE stack with a call to `sd_ble_gatts_service_add()`. Then, two characteristics are registered within the service in `disptxt_char_add()` and `demoscr_char_add()`. A write event handler for each characteristic is also registered at the same time. These event handlers are created in main.c and the pointers are passed during service setup as you can see above in `services_init()`.

```C
static void demoscr_write_handler(uint16_t conn_handle, ble_eink_t * p_eink, uint8_t scrnum)
{
    ...
}

static void disptxt_write_handler(uint16_t conn_handle, ble_eink_t * p_eink, uint8_t len, uint8_t *data)
{
    ...
}
```

#### Responding to BLE Events
When the EINK_UUID_DEMOSCR characteristic is written to, the demoscr_write_handler function is invoked and passed the 'scrnum' value supplied by the BLE Central client. Since the write handler is called from the BLE stack, the function cannot executed a blocking function call. So, only a flag is set and the action itself is taken in a different timer invoked function as below

```C
static void power_manage(void)
{
    ret_code_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
    switch(epaper_pending)
    {
        case 1:
            NRF_LOG_INFO("Starting to display text\r\n");
            epaper_demo_text();
            epaper_pending = 0;
            break;
        ...
            
        default:
            break;
    }
}
````

The call to epaper_demo_text() in the code snippet above executes the complete sequence of setting up the e-Paper SPI connection, initializing the display module, creating the filling the frame buffer and transferring the contents of the frame buffer.

```C
static void epaper_demo_text(void)
{
    APP_ERROR_CHECK(nrf_gfx_init(p_lcd));
    nrf_gfx_rotation_set(p_lcd, NRF_LCD_ROTATE_270);
    nrf_gfx_point_t text_start = NRF_GFX_POINT(15, 70);
    nrf_gfx_screen_fill(p_lcd, 0xff);
    APP_ERROR_CHECK(nrf_gfx_print(p_lcd, &text_start, 0x00, (const char *)disptxt_buffer, p_font, true));
    nrf_gfx_display(p_lcd);
    nrf_gfx_uninit(p_lcd);
}
```

The hardware initialized is done in the call to `nrf_gfx_init()`. The location to start drawing the text is set up in the variable text_start with the call to the macro NRF_GFX_POINT(). The screen is cleared to white using `nrf_gfx_screen_fill()`. The text is printed to the frame buffer with the call to `nrf_gfx_print()`. The frame buffer is transferred to the display using `nrf_gfx_display()`. The display is put to sleep and the nRF52 SPI resource released in `nrf_gfx_uninit()`.

#### Hooking up the GFX library for the Waveshare EPD

Nordic provides their own implementation of a GFX library. The library has primitives for drawing basic shapes like lines, rectangles and circles. It also allows rendering of BMP 565 encoded images on the screen. Here are some of the APIs

```C
void nrf_gfx_point_draw(nrf_lcd_t const * p_instance, nrf_gfx_point_t const * p_point, uint32_t color);
ret_code_t nrf_gfx_circle_draw(nrf_lcd_t const * p_instance,
                               nrf_gfx_circle_t const * p_circle,
                               uint32_t color,
                               bool fill);
void nrf_gfx_screen_fill(nrf_lcd_t const * p_instance, uint32_t color);
ret_code_t nrf_gfx_bmp565_draw(nrf_lcd_t const * p_instance,
                               nrf_gfx_rect_t const * p_rect,
                               uint16_t const * img_buf);
```

The GFX library is generic and needs to be implemented for a specific display by implementing a set of functions the library has to be initialized with. The pointers to those functions need to be populated into an instance of the nrf_lcd_t structure. In my code, I populate the structure in waveshare_epd.c

```C
const nrf_lcd_t nrf_lcd_wsepd154 = {
    .lcd_init = wsepd154_init,
    .lcd_uninit = wsepd154_uninit,
    .lcd_pixel_draw = wsepd154_pixel_draw,
    .lcd_rect_draw = wsepd154_rect_draw,
    .lcd_display = wsepd154_display,
    .lcd_rotation_set = wsepd154_rotation_set,
    .lcd_display_invert = wsepd154_display_invert, 
    .p_lcd_cb = &wsepd154_cb
};

```

The nrf_lcd_t structure contains pointers to seven functions that should be implemented to drive a display module. The first four are the minimum set that need to be implemented for any display to be functional. The other three would enhance the functionality but the library would work even without a full implementation, albeit with a reduced set of features.

* lcd_init has the function signature `ret_code_t (* lcd_init)(void)` and is called when the `nrf_gfx_init()` function is called. This function should set up the hardware and initialize any configuration parameters on the display module. It should return 0 if initialization succeeds. Otherwise, it should return a non-zero error code.

* lcd_uninit has the function signature `void (* lcd_uninit)(void)` and is called from `nrf_gfx_uninit()`. It should gracefully shut down the display if necessary and release any communication resources

* lcd_pixel_draw has the function signature `void (* lcd_pixel_draw)(uint16_t x, uint16_t y, uint32_t color)`. The function should draw a dot in the supplied color at the specified (x, y) location. The colors can be mapped from a 32bit number to an actual color supported by the display as desired in the implementation. In my implementation for the Waveshare EPD, the only two supported colors are 0 (zero) for black and anything non-zero for white.

* lcd_rect_draw has the function signature `void (* lcd_rect_draw)(uint16_t x, uint16_t y, uint16_t width, uint16_t height, uint32_t color)`. This function should fill in a rectangle of the given width and height at the location (x, y) in the specified color. Ideally, this would be an optimized implementation of a rectangle fill algorithm given the behavior and features of the device. The implementation I have included is far from optimized, filling the rectangle in one pixel at a time.

* lcd_display has the signature `void (* lcd_display)(void)`. It is called if there is a frame buffer involved, the contents of which should be optimally transferred to the display when the function is called.

* lcd_rotation_set sets a global rotation of the display with respect to the frame buffer. I have left this unimplemented in the current version

* lcd_display_invert would invert the colors of the pixels when called. I have left this unimplemented.


### Bare Minimum to control the e-Paper Display
The code for the project is doing far more than just control the e-Paper display, so it helps to outline the basic code flow required to control the display. Most of it is dealt with in the GFX library hookup code, but if you needed to customize certain aspects, you will be able to locate the relevant sections in waveshare_epd.c

#### Initialize display
The GFX library call that starts the initialization sequence is `nrf_gfx_init(p_lcd);`. This in turn calls `wsepd154_init()` which then initializes the GPIO and SPI and sets up the configuration registers on the display module. Check `hardware_init()` and `command_list()` in waveshare_epd.c for details of how that's done.

#### Create a frame buffer (image array of 1-bit depth)
All draw operations are first perform in a frame buffer of 1 bit depth. Since the screen is 200x200 pixels, it needs 40000 bits or 5000 bytes to represent it. 
`uint8_t screen_buffer[5000];`

#### Draw into frame buffer
The smallest draw unit is the pixel, represented by a single bit. So, all drawing operations operate on the pixel bit in the frame buffer. Here's the core of the `pixel_draw` function
```C
    uint16_t addr = x_ / 8 + y_ * ((WSEPD_WIDTH % 8) == 0 ? WSEPD_WIDTH / 8 : WSEPD_WIDTH / 8 + 1);
    uint8_t rdata = screen_buffer[addr];
    if(color == 0)
        screen_buffer[addr] = rdata & ~(0x80 >> (x_ % 8));
    else
        screen_buffer[addr] = rdata | (0x80 >> (x_ % 8));
```
The code above computes the byte location of the bit and sets or clears the specific bit depending on the 'color' being set.

#### Transfer frame buffer
The next step is to call `nrf_gfx_display(p_lcd)` which then calls `wsepd154_display()`. This function sets up the draw window and transfers the screen bitfields through the WRITE_RAM commands as defined in Waveshare's EPD Sepcification document available on their website. To be quite honest, I haven't fully deciphered the EPD commands as listed in the Specification document, so I'm simply replicating the code provided by Waveshare in their demo. Their Specification document doesn't go into the details of how each command works.

Here's the code excerpt for transfering the frame buffer.
```C
    width = (WSEPD_WIDTH % 8 == 0) ? (WSEPD_WIDTH / 8) : (WSEPD_WIDTH / 8 + 1);
    set_addr_window(0, 0, WSEPD_WIDTH, WSEPD_HEIGHT);
    for(i = 0; i < WSEPD_HEIGHT; i++)
    {
        set_cursor(0, i);
        write_command(WRITE_RAM);
        for(j = 0; j < width; j++)
        {
            addr = j + i * width;
            write_data(screen_buffer[addr]);
        }
    }
    turn_on_display();
```

#### Put display to sleep
After completion of drawing actions, it's a good idea to put the display to sleep for two reasons. Firstly, if the display stays charged, over time, the screen will get polarized and some of the color pigments will stick to one side. This will create ghosting effects. You can understand this better if you look up details of how electro-phoretic displays work. Second, the power consumption of the module drops when in deep sleep mode. You can also remove voltage to the module entirely to save further on power consumption.
In my code, this is done in `wsepd154_uninit()`:
```C
    write_command(DEEP_SLEEP_MODE);
    write_data(0x01);
```

And that's it. This should be enough to get a Waveshare e-Paper display running on an nRF52832.

### !! Not for production use !!
This project is strictly a demo. It is far from production-ready. The Bluetooth stack requires almost realtime response but the code provided here goes into blocking wait states for hundreds of milliseconds, which should never happen in production code. To convert this project into a production-ready version, the e-Paper drivers need to be implemented with an internal state machine driven with a timer and potentially an interrupt driver triggered on the transition of the Busy line. The SPI data transfer code can also be significantly streamlined to optimized performance instead of doing single byte transfers as it's done with the demo code. If you want to implement a production-ready e-Paper display driver for the nRF5x, please contact me directly.
